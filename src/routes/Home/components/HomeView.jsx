import React from 'react'
import logo from '../assets/logo.svg'
import classes from './HomeView.scss'

export const HomeView = () => (
    <div>
        <h1>MapORG</h1>
        <h4>Visual Organising</h4>
        <img alt='MapORG Logo' className={classes.logo} src={logo}/>
    </div>
)

export default HomeView
