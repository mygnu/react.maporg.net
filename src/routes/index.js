// We only need to import the modules necessary for initial render
import CoreLayout from '../layouts/CoreLayout/CoreLayout'
import Home from './Home'
import counterRoute from './Counter'
// import loginRoute from './Login'

const requireAuth = (store) => (nextState, replace) => {
    const state = store.getState()
    console.log(state)
    if (nextState.location.pathname === '/login') {
        return;
    }
    if (!state.auth || !state.auth.isAuthenticated) {
        replace({
            pathname: '/login',
            state: {
                nextPathname: nextState.location.pathname
            }
        });
    }
}
/*  Note: Instead of using JSX, we recommend using react-router
    PlainRoute objects to build route definitions.   */

// export const createRoutes = (store) => ({     path: '/',     component: CoreLayout, indexRoute:
// Home,     childRoutes: [counterRoute(store), testRoute(store)] })

export const createRoutes = (store) => ({
    path: '/',
    childRoutes: [
        // Home routes
        {
            component: CoreLayout,
            indexRoute: Home,
            childRoutes: [
                // loginRoute(store) // has for example /login /reset
            ]
        },
        // authed route root
        {
            component: CoreLayout,
            onEnter: requireAuth(store),
            childRoutes: [counterRoute(store)]
        },
        // etc.. other non authed routes HelpRoute, ContactRoute
    ]
})
/*  Note: childRoutes can be chunked or otherwise loaded programmatically
    using getChildRoutes with the following signature:

    getChildRoutes (location, cb) {
      require.ensure([], (require) => {
        cb(null, [
          // Remove imports!
          require('./Counter').default(store)
        ])
      })
    }

    However, this is not necessary for code-splitting! It simply provides
    an API for async route definitions. Your code splitting should occur
    inside the route `getComponent` function, since it is only invoked
    when the route exists and matches.
*/

export default createRoutes
