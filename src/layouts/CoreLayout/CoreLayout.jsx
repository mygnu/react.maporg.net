import React from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import Theme from './Theme'
// import Header from 'components/Header'
import HeaderBarAuth from 'containers/HeaderBarAuth'
import classes from './CoreLayout.scss'
// import 'styles/core.scss'

export const CoreLayout = ({children}) => (
    <MuiThemeProvider muiTheme={Theme}>
        <div>
            <HeaderBarAuth/>
            <div className='container text-center'>
                <div className={classes.mainContainer}>
                    {children}
                </div>
            </div>
        </div>
    </MuiThemeProvider>
)

CoreLayout.propTypes = {
    children: React.PropTypes.element.isRequired
}

export default CoreLayout
