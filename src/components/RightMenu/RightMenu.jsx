import React from 'react'
import IconButton from 'material-ui/IconButton'
import IconMenu from 'material-ui/IconMenu'
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert'
import MenuItem from 'material-ui/MenuItem'

export const RightMenu = (props) => (
    <IconMenu
        iconButtonElement ={
            <IconButton>
            <MoreVertIcon/>
            < /IconButton>
        }
        targetOrigin={{
            horizontal: 'right',
            vertical: 'top'
        }}
        anchorOrigin={{
            horizontal: 'right',
            vertical: 'top'
        }}>
        <MenuItem primaryText='Refresh'/>
        <MenuItem primaryText='Help'/>
            {props.loggedin &&
                <MenuItem
                    primaryText={'Log Out'}
                    onTouchTap={() => {props.logOut()}}/>
            }
    </IconMenu>
)

RightMenu.propTypes = {
    logOut: React.PropTypes.func.isRequired,
    loggedin: React.PropTypes.bool.isRequired
}

export default RightMenu
