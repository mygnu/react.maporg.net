import React, {Component, PropTypes} from 'react'
import AppBar from 'material-ui/AppBar'
import IconButton from 'material-ui/IconButton'
import MenuIcon from 'material-ui/svg-icons/navigation/menu'
import LeftDrawer from 'components/LeftDrawer'
import RightMenu from 'components/RightMenu'

class HeaderBar extends Component {
    static contextTypes = {
        router: PropTypes.object
    }
    static propTypes = {
        logIn: PropTypes.func,
        logOut: PropTypes.func,
        state: PropTypes.object
    }
    constructor(props) {
        super(props)
        this.state = {
            drawerOpen: false
        }
    }

    handleToggle() {
        this.setState({
            drawerOpen: !this.state.drawerOpen
        })
    }
    goToHome() {
        // this.props.setToken('test token')
        this.context.router.push('/')
    }
    render() {
        return (
            <div>
                <AppBar
                    title='MAPORG'
                    onTitleTouchTap={() => {
                        this.goToHome()
                    }}
                    iconElementLeft={
                        <IconButton onTouchTap = {
                                () => this.handleToggle()
                            } >
                            <MenuIcon/>
                            < /IconButton>
                        }
                    iconElementRight={
                        <RightMenu
                        loggedin = {
                            this.props.state.auth.loggedin
                        }
                        logOut = {
                            this.props.logOut
                        } />
                    }/>
                    <LeftDrawer
                        open={this.state.drawerOpen}
                        auth={this.props.state.auth}
                        onRequestChange={(drawerOpen) => this.setState({drawerOpen})}
                        logIn={(creds) => this.props.logIn(creds)}/>
            </div>
        )
    }
}
export default HeaderBar
