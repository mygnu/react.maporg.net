import React, {PropTypes} from 'react'
import Drawer from 'material-ui/Drawer'
import MenuItem from 'material-ui/MenuItem'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'
import {Circular} from 'components/Progress'
import ErrorMessage from 'components/ErrorMessage'

export default class DrawerMain extends React.Component {
    static propTypes = {
        auth: PropTypes.object,
        logIn: PropTypes.func,
        onRequestChange: PropTypes.func.isRequired,
        open: PropTypes.bool
    }
    constructor(props) {
        super(props)
        this.state = {
            showError: false,
            userError: '',
            passwordError: '',
            userName: '',
            password: ''
        }
    }
    handleUsernameChange(event) {
        this.setState({userName: event.target.value.trim()})
    }
    handlePasswordChange(event) {
        this.setState({password: event.target.value.trim()})
    }
    focusUsername(event) {
        event.preventDefault()
        this.setState({userError: '', showError: false})
    }
    focusPassword(event) {
        event.preventDefault()
        this.setState({passwordError: '', showError: false})
    }
    blurUsername(event) {
        event.preventDefault()
        if (event.target.value.trim().length <= 0) {
            this.setState({userError: 'username can\'t be empty or spaces'})
        }
    }
    blurPassword(event) {
        event.preventDefault()
        if (event.target.value.trim().length <= 0) {
            this.setState({passwordError: 'password can\'t be empty or spaces'})
        }
    }
    showError() {
        if (this.state.showError && this.props.auth.error) {
            return true
        }
        return false
    }
    handleLogin() {
        const creds = {
            username: this.state.userName,
            password: this.state.password
        }
        this.props.logIn(creds)
        this.setState({showError: true})
    }
    render() {
        return (
            <Drawer open={this.props.open} docked={false} onRequestChange={this.props.onRequestChange}>
                <TextField
                    hintText='username'
                    errorText={this.state.userError}
                    floatingLabelText='User Name'
                    onChange={(event) => this.handleUsernameChange(event)}
                    onFocus={(event) => this.focusUsername(event)}
                    onBlur={(event) => this.blurUsername(event)}/>
                <TextField
                    hintText='password'
                    errorText={this.state.passwordError}
                    type='password'
                    floatingLabelText='Password'
                    onChange={(event) => this.handlePasswordChange(event)}
                    onFocus={(event) => this.focusPassword(event)}
                    onBlur={(event) => this.blurPassword(event)}/>
                <RaisedButton fullWidth={true} primary={true} onTouchTap={() => this.handleLogin()}>Login</RaisedButton>
                <MenuItem>
                    Menu Item 2
                </MenuItem>
                {this.props.auth.fetching && <Circular/>}
                {this.showError() && <ErrorMessage>
                    {this.props.auth.error}
                </ErrorMessage>
}
            </Drawer>
        )
    }
}
