import React, {PropTypes, Component} from 'react'
import Paper from 'material-ui/Paper';
import {redA100} from 'material-ui/styles/colors'

const style = {
    height: 'auto',
    width: 'auto',
    maxWidth: '500',
    margin: 20,
    marginLeft: 'auto',
    marginRight: 'auto',
    padding: 15,
    textAlign: 'center',
    position: 'relative',
    zIndex: '999',
    backgroundColor: redA100
}
export default class ErrorMessage extends Component {
    static propTypes = {
        children: PropTypes.node
    }
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <Paper style={style} zDepth={4} rounded={true}>
                {this.props.children}
            </Paper>
        )
    }
}
