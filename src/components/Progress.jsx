import React from 'react'
import LinearProgress from 'material-ui/LinearProgress'
import CircularProgress from 'material-ui/CircularProgress'

export const Linear = () => (<LinearProgress mode='indeterminate'/>);
export const Circular = () => (<CircularProgress style={{padding: '80px'}}/>)
