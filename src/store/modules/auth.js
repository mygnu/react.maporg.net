// import {browserHistory} from 'react-router'
import axios from 'axios'
const baseUrl = 'https://maporg.net/api'
// ------------------------------------ Constants ------------------------------------
export const COUNTER_INCREMENT = 'COUNTER_INCREMENT'
export const LOGIN_PENDING = 'LOGIN_PENDNIG'
export const LOGIN_FAILED = 'LOGIN_FAILED'
export const LOGIN_DONE = 'LOGIN_DONE'
export const SET_TOKEN = 'SET_TOKEN'
export const SET_LOGGEDIN = 'SET_LOGGEDIN'
export const LOG_OUT = 'LOG_OUT'
export const SET_LOGIN_ERROR = 'SET_LOGIN_ERROR'

export const ID_TOKEN = 'ID_TOKEN'
// ------------------------------------ Actions ------------------------------------

export const logIn = (creds) => {
    // return (dispatch, getState) => {
    return (dispatch) => {
        dispatch({type: LOGIN_PENDING})
        axios({
            method: 'post',
            url: baseUrl + '/authenticate',
            data: creds
        }).then((res) => {
            localStorage.setItem(ID_TOKEN, res.data.id_token)
            dispatch({type: SET_TOKEN, token: res.data.id_token})
            dispatch({type: LOGIN_DONE})
            // browserHistory.push('/')
        }).catch((err) => {
            if (err.response) {
                dispatch({type: LOGIN_FAILED, error: err.response.data})
            } else {
                dispatch({type: LOGIN_FAILED, error: 'Network Error'})
            }
        })
    }
}

export const logOut = () => {
    return {type: LOG_OUT}
}
/*  This is a thunk, meaning it is a function that immediately
    returns a function for lazy evaluation. It is incredibly useful for
    creating async actions, especially when combined with redux-thunk!

    NOTE: This is solely for demonstration purposes. In a real application,
    you'd probably want to dispatch an action of COUNTER_DOUBLE and let the
    reducer take care of this logic.  */

export const authActions = {
    logIn,
    logOut
}

// ------------------------------------ Action Handlers ------------------------------------
const ACTION_HANDLERS = {
    [LOGIN_PENDING]: (state) => {
        return Object.assign({}, state, {
            fetching: true,
            error: null
        })
    },
    [LOGIN_FAILED]: (state, action) => {
        return Object.assign({}, state, {
            fetching: false,
            error: action.error
        })
    },
    [LOGIN_DONE]: (state) => {
        return Object.assign({}, state, {
            fetching: false,
            error: null,
            loggedin: true
        })
    },
    [SET_LOGIN_ERROR]: (state, action) => {
        return Object.assign({}, state, {error: action.error})
    },
    [SET_TOKEN]: (state, action) => {
        return Object.assign({}, state, {
            token: action.token,
            error: null
        })
    },
    [LOG_OUT]: (state) => {
        return Object.assign({}, state, {
            token: '',
            loggedin: false
        })
    }
}

// ------------------------------------ Reducer ------------------------------------
const initialState = {
    fetching: false,
    loggedin: true,
    token: null,
    error: null
}
export default function authReducer(state = initialState, action) {
    const handler = ACTION_HANDLERS[action.type]

    return handler
        ? handler(state, action)
        : state
}
